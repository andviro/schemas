#!/usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from uuid import uuid4
from schemas.edi import ContainerEDI


def test_soft_version():
    global ti
    # Создание контейнера "с нуля"
    # в параметрах конструктору можно передавать начальные значения полей
    # для непереданных полей присваиваются значения по умолчанию
    # неприсвоенные поля без умолчаний бросят исключение при попытке вызвать
    # .save() или преобразовать контейнер в XML.
    ti = ContainerEDI(uid=uuid4().hex,
                      doc_code='20',
                      trans_code='01',
                      soft_version='XML ORM v0.6',
                      )
    ti.sender = ti.Sender(uid=uuid4().hex)
    ti.receiver = ti.Receiver(uid=uuid4().hex)
    ti.sos = ti.Sos(uid=u'2AE')
    for n in range(3):
        d = ti.add_file(filename='some{0}.xml'.format(n),
                        doc_type='0{0}'.format(n + 1),
                        content_type='xml', content='test content {0}'.format(n).encode('ascii'),
                        signatures=['test sign {1} content {0}'
                                    .format(n, s).encode('ascii')
                                    for s in range(3)],
                        sig_role=u'спецоператор')
        print(d.type)
    # сохранение сработает, только если контейнер сформирован корректно
    assert ti.raw_content
