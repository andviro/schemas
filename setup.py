#!/usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import sys

try:
    from setuptools import setup
    has_setuptools = True
except ImportError:
    from distutils.core import setup
    has_setuptools = False


setup_params = dict(name='edo_schemas',
                    version='0.0.6',
                    packages=[str('schemas')],
                    author='Andrew Rodionoff',
                    author_email='andviro@gmail.com',
                    license='LGPL',
                    description='Transport container schemas for xml_orm',
                    )

setup(**setup_params)
